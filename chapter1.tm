<TeXmacs|2.1>

<style|<tuple|generic|british>>

<\body>
  <doc-data|<doc-title|Introduction: Of Topology, Disorder and Localization>>

  <section*|R�sum� en Fran�ais>

  Dans ce chapitre, j'introduis les concepts qui seront discut�s tout au long
  de la th�se. L'introduction des notions sert �galement de pr�texte pour
  pr�senter plusieurs exp�riences que j'ai trouv�es assez belles et qui
  mettent en �vidence la vari�t� des exemples dans lesquels la topologie peut
  se manifester.

  Je commence d'abord par motiver ce travail en rappelant l'importance des
  semi-conducteurs dans la soci�t� industrielle du XXI�me si�cle. Des
  ordinateurs aux panneaux solaires, ce sont les briques �l�mentaires d'une
  quantit� ph�nom�nale de technologies. J'explique ensuite comment leur
  comportement particulier peut �tre �tudi� gr�ce � la th�orie des bandes.
  Cette derni�re permet une repr�sentation synth�tique des niveaux d'�nergie
  d'un mat�riau : on sait ainsi � quelles �nergies un �lectron peut, ou pas,
  se propager. Il est possible de cr�er des espacements entre ces bandes
  d'�nergies autoris�es : des bandes interdites, on parlera souvent de
  ``gap". Je termine cette section en pr�sentant le c�l�bre effet Hall
  quantique ainsi que son analogue classique. L'effet de spin Hall quantique
  et classique est �galement abord�. Ces deux effets permettent d'entrevoir
  une premi�re sp�cificit� des syst�mes topologiques : la circulation des
  �lectrons se fait sur le bord de l'�chantillon, ils n'ont pas le droit de
  p�n�trer � l'int�rieur. Cela assure que la propagation se fait dans un seul
  sens et donc sans r�tro-diffusion.

  Je fais ensuite la transition entre les syst�mes constitu�s de particules
  et les ondes, j'explique ce que j'entends par la localisation d'une onde :
  l'essentiel de l'�nergie de l'onde se trouve dans une zone restreinte de
  l'espace. On peut aussi �tudier la localisation comme la probabilit� de
  retour d'une onde ; une onde est dite localis�e si, dans le temps, elle
  revient souvent au m�me endroit dans l'espace. Je fais �galement un court
  apart� sur la localisation d'Anderson, une situation o� c'est le d�sordre
  qui permet la localisation des ondes, pour enfin finir sur une pr�sentation
  de simulations num�riques qui permettent d'appr�hender intuitivement les
  concepts qui se cachent derri�re la localisation.

  Ensuite, la topologie est introduite en tant qu'outil math�matique pour le
  physicien. Un outil qui permet de donner des d�finitions pr�cises au
  concept de trou dans une surface, par exemple. Pour cela, j'introduis la
  notion d'espace contractile : un espace qui peut �tre contin�ment d�form�
  en un point, et la formule d'Euler est introduite sans formalisme lourd.
  Ensuite, c'est au tour du concept de surface orientable d'�tre d�fini, puis
  celui de genre, notre premier invariant topologique : un nombre qui
  caract�rise globalement notre espace. Le lien avec la notion de trou dans
  une surface est fait, puis on s'int�resse au moyen de calcul de notre
  invariant topologique. Pour cela, on introduit la courbure de Berry et la
  formule de Gauss-Bonnet. Je fais ensuite le lien entre ces notions purement
  math�matiques et la physique en introduisant le nombre de Chern. Cet
  invariant, tr�s utilis� en physique, caract�rise un fibr� vectoriel
  construit � partir de la zone de Brillouin d'un syst�me p�riodique et du
  Hamiltonien du syst�me. Ce calcul est au c�ur du travail effectu� dans
  cette th�se. Je conclus la section en expliquant le lien entre �tat de
  bords et topologie.

  Finalement, dans la derni�re section, on parcourt un �ventail d'exp�riences
  qui mettent en �vidence l'existence d'�tats de bords localis�s sur les
  bords et r�sistant au d�sordre. Par exemple, on �tudie des syst�mes de
  gyroscopes coupl�s les uns aux autres. Ce probl�me, qui peut �tre trait�
  comme un probl�me de m�canique du solide newtonienne, pr�sente des
  propri�t�s topologiques inattendues. On regarde aussi un syst�me
  astucieusement construit pour faire appara�tre de la topologie dans les
  ondes acoustiques, ou encore au sein d'ondes plan�taires. Des ondes qui se
  produisent dans les oc�ans sur des distances de l'ordre de plusieurs
  centaines de kilom�tres. Pour des raisons que j'explique en d�tail, ces
  ondes se retrouvent \Ppi�g�es\Q autour de l'�quateur, incapables de
  traverser. Cela sert aussi de pr�texte pour introduire des analogies
  classiques � des principes normalement issus de la physique quantique,
  comme l'effet Aharonov-Bohm, l'effet Zeeman, ou encore le spin.

  \;

  Topology is a branch of mathematics that focuses on objects globally.
  Unlike geometry which studies objects locally, with topology, we are
  interested at properties of the objects which are the emnation of the
  complete structure. \ An image that is often use to introduce the concept
  of topology in layman terms, is the equivalence, for a topologist, between
  a mug and a donut<\footnote>
    A lot of jokes on the Internet depict topologist drinking coffee into a
    donut-shaped mug.
  </footnote>. It is indeed intuitive to see how you can smoothly deform one
  into the other without tearing or gluing any pieces of the surface, only
  stretching, twisting or bending. I gave here a first example of a typical
  topological invariant: the number of holes in a 3D surface. This number is
  called the genus and is zero for a sphere, one for a torus, two for a Klein
  bottle, <text-dots> Even though this picture can be useful to make the
  public understand basic concepts such as continuity, local or global
  properties or even equivalence class, I think that making the connection
  between this way of seeing topology and what actually happens in physical
  systems such as those studied in this work can be quite hard. This is why I
  will try to give here an intuition of why we consider a system to be
  \Ptopological\Q and how topology proves highly effective in constructing
  physically robust systems in the presence of imperfections or disorder.
  Doing so, I will also motivate this work by briefly surveying the
  literature in this area. I do not aim to provide a comprehensive
  state-of-the-art review but rather to shed light on some interesting or
  surprising systems.

  <section|Semi-conductors: bricks of the modern world>

  Before diving into the realm of topology, let's focus a bit on another vast
  field of physics: semi-conductors. Because of their presence in
  transistors, they are now everywhere, in your phone, in your computer,
  probably in your watch, also in your car. They are fundamental to build
  solar pannels, LED or photo-detectors. This is why they are the essential
  bricks of most of the modern electronic devices [].\ 

  Wondering what a semi-conductor leads us to wondering what a conductor is.
  One could simply say that a conductor \Pis not an insulator\Q. As I did not
  defined an insulator, this definition would be pointless, so we should
  remember of this fact more of a property than a proper definition. Some
  materials are know to carry currents, this is the case of metal per
  example. A very naive model to describe the transport of currents through a
  metal is the Drude model[]. In this model, electrons move freely in the
  metal, following the laws of classical mechanics, bouncing against the ions
  of the metal like in a pinball machine. The more impureties are present in
  the metal, the more they are slowed down, making the metal less conductive,
  and thus more resistant. Surprisingly the relation between the amount of
  impurities and the conductivity is not linear and drops drastically at a
  certain rate of impurities, making the metal isolant. The reason behind
  this phenomenum is called Anderson localization [] and is not the main
  subject of this work, but we might touch upon it from time to time. To sum
  up: electrical conductors are materials that allow current to flow by
  enabling charge carriers to move, whereas in insulators, nothing happens,
  and no current flows. Of course this definition is not perfect and would
  not suit up to exotic situations but I suggest that we settle for this at
  the moment, as this work does not require a more complex definition.

  Now that we agree on what \Pinsulators\Q and \Pconductors\Q are, we can can
  focus on semi-conductor that are sometimes conductor, sometimes insulators.
  Their nature depends often of a physical parameter such as temperature or
  internal disorder. This feature of semi-conductor to let or not pass the
  current allows for a very wide range of applications, the most known being
  probably transistors where a small electric field can control a way bigger
  current. Here, the electric field plays a role at an atomic scale, to
  understand what happens we need to forget about the Drude model and have a
  look at what quantum mechanics says. We know that the energy of an atom is
  quantified, only precise values of energy are allowed for an atom, per
  example <math|E<rsub|1>,E<rsub|2>,\<ldots\>,E<rsub|n>>. When many atoms are
  put together, these precise values tends to enlarge and the ensemble can
  take values in intervals: <math|\<Delta\>E<rsub|1>,\<ldots\>,\<Delta\>E<rsub|n>>.
  These intervals are called \Pbands\Q and the forbidden energy zones between
  bands are called \Pband gaps\Q or simply \Pgaps\Q. As we want to make our
  materials conductors, we want to be sure that an electron will be allowed
  to go through it. I introduce here the concept of \PFermi level\Q as the
  exchange of energy required to add one electron to the system. In a
  semi-conductor, the band that is situated just below the Fermi level is
  called the valence band and the one above is called the conduction band and
  these two bands are separated by a gap that is small enough so that
  applying a small electric field will be enough to move electrons from the
  valence band to the conduction band allowing the current to flow.

  <\big-figure|<image|figs/hall_effect.pdf|0.91par|||>>
    <math|<around*|(|a|)>> The classical Hall effect: by applying a magnetic
    field <math|<math-bf|B>=B*e<rsub|z>> on a slab where electrons circulate
    along the <math|x> axis, the Lorentz force deviate them along the
    <math|y> axis creating a tension <math|V<rsub|H>> and thus a resistance
    <math|R<rsub|H>=V<rsub|H>/I>. <math|<around*|(|b|)>> The quantum Hall
    effect (QHE), the setup is similar but at low temperature and the
    magnetic field has to be very strong (1-10 T). Electrons in the bulk are
    blocked in circular orbits, only electrons on the edges can carry a
    current. <math|<around*|(|c|)>> The spin Hall effect, due to intrinsic
    properties of the material, spin up and down are treated differently by
    spin-orbit interactions, leading to a spin accumulation on lateral faces
    of the material. <math|<around*|(|d|)>> The quantum spin Hall effect
    (QSHE)
  </big-figure>

  So far, we have discussed materials whose insulating or conducting
  character evolves over time, but there are also materials that are both
  insulating and conducting in different regions of space. Often, they are
  insulating in the bulk, which is the core or interior of the sample, and
  conducting at the edges. These are known as topological insulators. For the
  moment, it is not at all obvious what makes them topological. We will
  address this later. Let's focus on the description of the most well-known
  of these materials: the quantum Hall effect (QHE) with first a small detour
  by the Hall effect.

  The Hall effect was discovered by Edwin Hall in 1879. A current <math|I>
  goes through a conducting material, carried by electrons at velocity
  <math|*<math-bf|v>>. Then, a perpendicular magnetic field
  <math|<math-bf|B>> is applied to the current. The Lorentz force deviates
  the electrons in a direction perpendicular to the magnetic field,
  consequence of the expression of the force:
  <math|<math-bf|F>=q*<math-bf|v>\<times\><math-bf|B>>. This deviation
  creates an electric field and then a Hall voltage <math|V<rsub|H>> that can
  be measured to retrieve the intensity of the magnetic field. I also
  introduce here another quantity: the Hall resistance
  <math|R<rsub|H>=V<rsub|H>/I>.

  The QHE is quite similar: we take the same setup but we now consider
  electrons only one plane. Some author refer to this plane as a \Ptwo
  dimensionnal electron gas\Q (2DEG). The experiment also takes place at very
  low temperature (near 0K) and strong magnetic fields are required. Because
  of the strong magnetic field, electrons are forced to follow circular
  orbits<\footnote>
    Note that this is quite important for the electrons to be confined in 2D
    otherwise they would just leave the sample in a cyclotron motion.
  </footnote> and their energy is now quantized: it can only take equidistant
  values called Landau levels. The consequence of this quantization is that
  the Hall resistance is now quantized and equals <math|h/e<rsup|2>\<nu\>>
  where <math|h> is the Planck constant, <math|e> the elementary charge and
  <math|\<nu\>> is an integer. As the electrons in the bulk are blocked in
  circular orbits, they cannot be used as charged carriers, thus only the
  electrons on the edges can play this role. The conducting states are
  localized on the edge of the sample. This property is highly intriguing as
  it ensures that electron transport will occur without backscattering, and
  heat losses will be low to the point of being negligible. The reason for
  this last point to happen is that if the electrons are allowed to move only
  in one direction then there is no interaction with backscattered electrons,
  yet this is the reason of heat loss.

  Despite being very interesting, the QHE is hard to achieve outside of a
  laboratory. The strong magnetic fields required could not be created in
  small devices. One avenue considered to eliminate the need for these
  intense magnetic fields is the quantum spin Hall effect (QSHE). By using
  particles that possess both spin up and spin down, we can take advantage of
  spin-orbit coupling to \Pseparate\Q the paths of up and down particles.

  <section|Localization and disorder >

  In the previous section I have been mentioning the concept of localization
  several times. This is a very vast and complex matter and I will try to
  give here just a rough idea of what we are refering to when mentionning it.
  I choosed to introduce this concept along with the one of disorder as we
  will see that for waves they are somehow linked.

  We can define localization for a cluster of particles or for a wave. Saying
  that these particles or waves are localized is equivalent to stating that
  there is an absence of diffusion on their part. In other words, for an
  arbitrarily long time <math|t>, the probability of finding a particle or
  the energy density of the wave outside a defined region of space that is
  smaller than the entire space is very low. This is, more or less, the
  definition of localization given by Anderson himself in his 1958's paper.
  However this definition requires to deal with time evolution and averaging
  of the system and that's why there exists other definition [bart] that
  avoid it and give a more formal approach of things. I believe that here it
  is possible to stick to this \Phands-on\Q definition because the phenomena
  of localization are not studied in a very precise manner in this work.

  An interesting tool when it comes to studying localization is the return
  probability <math|P<around*|(|<math-bf|r>|)>>, the probability for a
  particle to come back at point <math|<math-bf|r>> when leaving
  <math|<math-bf|r>>. For example if we consider a particle folowing a random
  walk in one dimension so that the probability of going right or left at
  each step is <math|1/2>. It is possible to prove that the particle will
  return at the point of depart at least one time almost surely. A
  consequence of this statement is that it will return an infinite number of
  time at any point. These statement stays true in two dimension but become
  false in three or higher dimensions. I will define later in this text the
  equivalent of the return probability for a wave function
  <math|\<psi\><rsub|n>> and will call it the inverse participatio ratio,
  after this short discussion, I hope that the intuition behind this concept
  will be clear.

  Even though Anderson localization is not the center subject of this work, I
  cannot resist to give here a short introduction on the matter as we will
  encounter this concept a few times over this manuscript. As mentionned
  previously Anderson localization arises when the disorder in a sample reach
  a certain rate. \ Then the mean free path of the wave can become smaller
  than the wavelength forbiding it to spatially extend. It has been proven to
  occur in one or two dimension for scalar and vector waves, however in three
  dimension it has been observed only for scalar waves but recent work [hui
  cao] tends to indicate that it might occur also for vector waves. One
  should remember that localization is a phenomenum that can be very hard to
  observe experimentally as it can compete with a lot of ther effect such as
  absorption or fluorescence. In this work we will encounter two dimensionnal
  Anderson localization in disordered photonic system.

  There are two aspects of disorder that we will explore in this work. The
  first one is the most expected effect: disorder disrupts the structure and
  consequently, the phenomena created by that structure. The resistance of a
  material's properties to disorder is a key point in determining if the
  material can be easily produced in large quantities and used under
  conditions that may compromise its internal structure. On the other hand,
  disorder has the ability to trigger intriguing effects under specific
  conditions. An example of this is Anderson localization, which has been
  mentioned previously.\ 

  In the following, I delve into the waves of water, found in the sea or
  rivers, for instance. These waves can be described by the following wave
  equation if we note <math|u<around*|(|<math-bf|x>,t|)>> the height of a
  water wave, then it obeys:

  \;

  <\equation>
    <frac|\<partial\><rsup|2>u<around*|(|<math-bf|x>,t|)>|\<partial\><rsup|2>t>=c<rsup|2>\<Delta\>u<around*|(|<math-bf|x>,t|)><label|wave-equation>
  </equation>

  \;

  Numerous papers show how random media are good at trapping these kind of
  waves, a practical example of this comes from biology: mangrove, they have
  been proved to be highly effective when stopping waves coming from tsunami
  or cyclone [<slink|https://link.springer.com/article/10.1007/s11273-005-0613-4>
  <slink|https://iopscience.iop.org/article/10.1016/S0169-5983(98)00024-0>
  <slink|https://doi.org/10.1016/j.cub.2005.06.008>]

  It appears that <reference|wave-equation> can numerically be solved by the
  finite-difference method and by doing so in a disordered medium, one can
  observe Anderson localization phenomena. To visualize it I highly recommend
  to have a look at the work of Nils Berglund, a
  <hlink|playlist|https://www.youtube.com/watch?v=-AM6z5a9BIw&list=PLAZp3rbgWLo2VvXUsaiRbw33x2qMKASdF>
  is available on youtube showing how incident waves react to various
  lattices. These nicely done animation provide good intuition on how waves
  propagate in lattices.

  <section|Topology in a nutshell>

  <\big-figure|<image|figs/elem_cells.pdf|0.93par|||>>
    <label|elem_cells>Decomposition of surfaces into elementary celles
    (contractible surfaces). The circle <math|<around*|(|a|)>> is decomposed
    in a point and a segment <math|<around*|(|b|)>> wich are both
    contractible. The torus <math|<around*|(|c|)>> is decomposed in an open
    cylinder and a circle <math|<around*|(|d|)>>, the open cylinder can be
    reduce to a circle, both circle reduced to points and segments.
    <math|<around*|(|e|)>> presents another decomposition of the torus by
    cutting y transversely, despite being different these two decompostions
    end up computing the same characteristic of Euler:
    <math|\<chi\><around*|(|Torus|)>=0>. <math|<around*|(|f|)>> presents a
    decomposition of the sphere into elemntary cells.
  </big-figure>

  In the beginning of this chapter, I gave an example of \ topological
  invariant: a number that is conserved for any smooth deformation on a
  surface. By smooth deformation I mean that it is forbidden to tear, cut or
  glue the surface or the volume. This number is called the genus and can be
  seen as the number of \Pholes\Q or \Pances\Q in a surface. This definition
  is easy to understand but not very formal and a bit subjective, indeed if
  you dig in your garden for a certain amount of time you would end up with
  something that you would probably call a \Phole\Q ! But a topologist would
  strongly disagree with you, this is why I will try to give here a more
  precise intuition behind their definition yet I will not use the heavy
  machinery of mathematicians.\ 

  The first notion I want to introduce here is the one of \Pcontractible
  space\Q. A space is said to be contractible if it can be continuously
  shrunk to a point. A segment is contractible but a circle is not. However a
  disc is contractible as much as the volume inside a sphere, but not the
  sphere itself. A surface torus but also a volume torus are not
  contractible.\ 

  Any three dimensional object can be decomposed in a set of elementary cells
  that are all contractibles: points, \ segment, surfaces and volumes. For
  example a circle can be seen as a point and a segment. A surface sphere can
  be seen as a circle and two caps, thus as a point a segment and two caps
  because the circle is not elementary. Think about how you can decomposate a
  torus (surface and volume). The figure illustrates those decomposition.
  Once we have done such decomposition for an object, we can compute its
  Euler characteristic <math|\<chi\>>:

  <\equation>
    \<chi\>=N<rsub|P>-N<rsub|L>+N<rsub|S>-N<rsub|V>
  </equation>

  With <math|N<rsub|P>> the number of point, <math|N<rsub|L>> the number of
  segments, <math|N<rsub|L>> the number of elementary surfaces and
  <math|N<rsub|V>> the number of elementary volumes. Note that the elementary
  decomposition we mention sooner also work for higher dimension than three
  and then the formula to compute the Euler characteristic generalizes as an
  alternate sum of \Pnumber of surfaces\Q and \Pnumber of volumes\Q for
  increasing dimension. With the previous decomposition we have done, we
  obtain that <math|\<chi\><around*|(|Circle|)>=1-1=0>,
  <math|\<chi\><around*|(|Sphere|)>=1-1+2=2> and
  <math|\<chi\><around*|(|Surface Torus|)>=2-2=0>, note that the Euler
  characteristic does not depend of the decomposition we choose. Figure
  <reference|elem_cells> shows several decomposition for the torus. It is
  also possible to convince ourselves that if an object <math|O<rsub|1>> is
  the merge<\footnote>
    By merge I mean a disjoint union.
  </footnote> of two objects <math|O<rsub|2>> and <math|O<rsub|3>> then
  <math|\<chi\><around*|(|O<rsub|1>|)>=\<chi\><around*|(|O<rsub|2>|)>+\<chi\><around*|(|O<rsub|3>|)>>
  as the decomposition will also sum up.\ 

  For the following, to keep it simple, I will focus only on surfaces in
  <math|\<bbb-R\><rsup|3>> that are orientable, compact and without
  boundaries. Non orientable means that there is no way to differentiate
  between rotationing clockwise and anticlockwise. For example in
  <reference|zoology>, only <math|<around*|(|a|)>> and <math|<around*|(|b|)>>
  fit. <math|<around*|(|c|)>> the Klein bottle has no boundaries but is not
  orientable, <math|<around*|(|e|)>> is orientable but has boundaries and
  <math|<around*|(|d|)>> is not orientable and has boundaries. We can now
  define the genus <math|g> using the following relation for a surface
  <math|S> in <math|\<bbb-R\><rsup|3>>:

  <\equation>
    \<chi\><around*|(|S|)>=2<around*|(|1-g|)>
  </equation>

  And notice that it matches the instinctive \Pnumber of holes\Q we would
  have seen in a sphere or a torus. Now that we have properly defined the
  notion of holes in a surface, It would be nice to have a tool that can
  ensure a straightforward computation without tedious decomposition. A way
  to do this is to compute the curvature <math|\<Omega\><around*|(|<math-bf|x>|)>>.
  The curvature is a local operator, if we smootly deform the surface
  <math|S> in a local neighbourhood of <math|<math-bf|y,>> the curvature will
  change for any <math|<math-bf|x>> within the neighbourhood. However it
  appears that globally, if we integrate the curvature over the complete
  surface it remains unchanged such that for any orientable compact surface
  <math|S> with no boundaries, we have:

  <\equation>
    <big|int><rsub|S>\<Omega\><around*|(|<math-bf|x>|)>\<mathd\><math-bf|x>=2\<pi\>\<chi\><around*|(|S|)>
  </equation>

  An example of curvature is the Gauss curvature defined as the product of
  the two inverse of the biggest and smallest radii of curvature at point
  <math|<math-bf|x>>. The intuition between curvature can also be given by
  having a look at the holonomy: we choose a closed loop and we transport a
  vector adiabatically of this loop, when we arrive at the end of the loop,
  we compute the angle between the starting vector and the ending vector, the
  angle is the holonomy of the path. For a plane, no matter the path,
  holonomy will always be zero, however for a sphere, as demonstrated in
  <reference|zoology>, if we follow a path that approximately goes arround a
  quarter of the sphere, we finish with holonomy to be equal to
  <math|\<pi\>/2>. In the M�bius strip example it's even worse, the path we
  choose completely encircles the strip, doing the same on the sphere would
  end up with the holonomy equals to zero but as the M�bius strip is non
  orientable we end up with holonomy equals to <math|\<pi\>>.

  <\big-figure|<image|figs/genus.pdf|0.9par|||>>
    <label|zoology>Zoology: <math|<around*|(|a|)>> Sphere: <math|g=0>
    <math|<around*|(|b|)>> Torus: <math|g=1> <math|<around*|(|c|)>> Klein
    botle: <math|g=2> the Klein bottle is made of two M�bius strip sticked
    together, it is a non orentable-shape and thus the genus we refer two is
    slightly different from the one we define in the body of the text.
    <math|<around*|(|d|)>> M�bius strip is also a non-orientable shape.
    <math|<around*|(|e|)>> Simple strip.
  </big-figure>

  At this juncture, a legitimate question might arise: How does this relate
  to physics? To establish this connection, we need to recall the concept of
  bands within semiconductors, which represent the permissible energies. For
  each band, every energy level within it can span a space alongside its
  associated eigenfunctions. The set of all these spaces spanned by each
  energy level within the band constitutes what is known as a fiber bundle.
  This fiber bundle will serve as the surface upon which we compute
  topological invariants. I will refer to it as the Chern number. Similar to
  the computation of the Euler characteristic, the Chern number can be
  computed by integrating the Berry curvature (as opposed to the Gauss
  curvature) over a surface known as the Brillouin zone.

  It is important to note that in this instance, we have added a layer to our
  process. Previously, we computed our topological invariants by directly
  integrating the curvature on the surface. Now, we generate a fiber bundle
  from the surface (via the Hamiltonian) and compute the topological
  invariant for this fiber bundle.

  This is where we begin to appreciate the utility of topology. Similar to
  how the genus of a surface remains unchanged unless there is an abrupt
  transformation, the Chern number of a band remains constant as long as that
  band doesn't connect to another band, closing the gap between them. This is
  why we designate certain materials as ``topological insulators". As long as
  the gap remains open, these materials exhibit topological properties,
  consequently harboring states on their edges.

  Now, an essential question arises: Why do edge states emerge? The reason is
  relatively simple\Vwhile the material is topological, the vacuum
  surrounding it is trivial. To transition from one to the other, the gap
  must be closed, giving rise to the emergence of edge states.

  <section|From electrons to photons>

  The examples I have provided so far mainly focus on electron movements. In
  reality, topological effects in wave physics occur in many other instances
  such as acoustics, mechanics or electromagnetics, the last being the
  primary focus of this thesis. \ But before diving into that, let us explore
  other examples.

  <slink|https://comptes-rendus.academie-sciences.fr/physique/item/10.5802/crphys.3.pdf>

  <subsection|Mechanical waves>

  https://www.pnas.org/doi/full/10.1073/pnas.1507413112

  Studying physics often commences with an exploration of Newtonian
  mechanics, which is precisely our focus in this subsection. We'll
  investigate a system that can be described by classical motion equations
  yet exhibits intriguing topological properties.

  For mathematical reasons that we shall explain in [chapter 2], topological
  states often appear when the time-reversal symmetry (TRS) is broken.
  However Newton's law of motions state that for an object of mass <math|m>,
  the sum of forces that appy on the object <math|<math-bf|F>> is given by
  the equation:

  <\equation>
    <math-bf|F>=<frac|\<mathd\><math-bf|p>|\<mathd\>t><label|newton>
  </equation>

  where <math|<math-bf|p>=m*<math-bf|v>>. This equation is second-order in
  time, so the change of variable <math|t\<rightarrow\>-t> leaves it
  unchanged. To break TRS we need to resort to a little trick: using angular
  momentum.\ 

  It is impossible to balance a spinning top on its axis <math|<math-bf|J>>
  when it's motionless, but it becomes possible when it spins. In this
  scenario, a torque force is generated, preventing the top from falling
  over. We consider the angular momentum <math|<math-bf|L>=<math-bf|J>\<times\><math-bf|p>=I*\<omega\><math-bf|J>/J>,
  then if we attach the tail of the axis <math|<math-bf|J>> and we apply a
  force <math|<math-bf|F>> to the other end, by derivating the angular
  momentum and using <reference|newton> we end up with the relation:

  <\equation>
    <frac|\<mathd\><math-bf|J>|\<mathd\>t>=<frac|J|I\<omega\>><math-bf|J>\<times\><math-bf|F>
  </equation>

  which is first order in time ! We now have a plan: create a structure made
  of masses interconnected by springs, this kind of structure might represent
  physical systems such as bridges, but instead of simples masses they would
  be gyroscope. By doing so the answer of each node to a force is first order
  in time and TRS can be broken.

  This system was experimentally realized in [reference] by employing small
  DC motors connected to the ground via springs. Instead of using springs to
  interconnect the gyroscopes, they utilized neodymium magnets aligned
  vertically. For small displacements, the repelling force of the magnets
  resembles that of Hooke's law. They then arranged these cells in a
  honeycomb lattice. The choice of a honeycomb lattice is not arbitrary and,
  as we will explore in this thesis, it is commonly employed to fabricate
  materials with topological properties. Noteworthy features of such networks
  include ensuring that each cell has precisely three nearest neighbors and
  exhibiting several symmetries.

  By exciting the system at multiple frequencies, it becomes evident that
  there are modes propagating solely along the edge. This means that only the
  gyroscopes at the edge experience a change in their axis of inclination,
  <math|<math-bf|J>>. Apart from observing these modes in numerical
  simulations, their presence is also evident in the experiment. This
  demonstrates that these modes are robust against disorder induced by
  experimental constraints, such as slightly different motor rotation speeds.
  The resilience of a mode in the face of disorder is a highly sought-after
  property when aiming to create systems that propagate waves reliably and
  without energy loss, it constitues an intrinsic characteristic of
  topological systems.

  The attentive reader will notice that what we created here is very similar
  to the quantum spin hall effect described earlier, but here it's even
  better because the spin is really \Pspinning\Q and is not just an image for
  an abstract property of atoms.

  <\big-figure|<image|figs/mechanical_waves.pdf|0.99par|||>>
    <math|<around*|(|a|)>> A ring filled with a fluid, such as air in the
    experiment, allows the propagation of sound. In the static state, the
    ring hosts two counterpropagating modes with identical frequencies.
    However, when the ring rotates at speed <math|v>, the mode moving in the
    direction of the ring's rotation experiences an increase in frequencies,
    while the other mode experiences a decrease. Consequently, this action
    lifts the degeneracy between the two modes. <math|<around*|(|b|)>> An
    example of honeycomb lattice. <math|<around*|(|c|)>> Masses attached to
    the ground via springs rotate at a frequency of 300Hz due to a DC motor.
    A powerful neodymium magnet creates repulsion between the rotating
    masses, forming a system assembled in a honeycomb lattice.
    <math|<around*|(|d|)>> A lattice consisting of gyroscopes interconnected
    by springs. Each gyroscope experiences three forces from the three
    springs it's attached to, along with a torque resulting from its internal
    rotation.
  </big-figure>

  <subsection|Acoustic waves>

  It is also possible to observe topological effects in acoustic waves.
  However, breaking time-reversal symmetry (TRS) for these waves can be
  challenging; it's often energy-intensive and visible in very large samples,
  requiring the introduction of non-linear effects, for example. Yet,
  breaking TRS is a relatively direct way to introduce topology. One
  solution, proposed by Fleury et al., involves using ring resonators capable
  of rotating asynchronously, which are filled with a fluid enabling sound
  propagation. When the average circumference of the ring resonates an
  integer number of times the wavelength, it creates two counterpropagating
  modes. By applying an air flux across the system, the rings rotate freely
  according to the airflow, imparting angular momentum. Consequently, one
  mode travels in the same direction as the ring's velocity while the other
  moves against it, causing their velocities and frequencies to
  degenerate\Vreminiscent of a classical analogue of the Zeeman effect,
  discovered in 1896 by Pieter Zeeman. This effect shows how, in a presence
  of a magnetic field, a spectral line can see split itself. \ This splitting
  is linear, making it a perfect analoguous for the system aforementionned,
  and comes from the degeneracy lift of one of the transition.

  https://www.nature.com/articles/s42005-018-0094-4 topological sound

  <slink|https://arxiv.org/pdf/1503.06808.pdf> Observation of phononic
  helical edge states in a mechanical `topological insulator'

  <subsection|Equatorial waves>

  <subsubsection|Aharonov-Bohm effect>

  Another types of waves that are subject to topological effects are water
  waves. It has been discovered in the seventies that surface waves dislocate
  [] when encoutering an irrotational vortex, a classical equivalent of the
  well-known Aharonov-Bohm (AB) effect. This last one was studied in 1959 []
  and provided evidence that the matter in quantum mechanics when studying
  particles under the influence of a magentic field <math|<math-bf|B>>, was
  not the field itself, but the potential vector <math|<math-bf|A>> such that
  <math|<math-bf|B>=\<nabla\>\<times\><math-bf|A>><\footnote>
    This statement is easy to verify when writing the hamiltonian of a
    particle of mass <math|m> and charge <math|q> in a magnetic field:\ 

    <\equation*>
      <wide|H|^>=<frac|<around*|(|<wide|p|^>-q<math-bf|A><around*|(|<wide|<math-bf|r>|^>|)>|)><rsup|2>|2m>
    </equation*>
  </footnote>. Indeed, in the AB effect, a beam of particles of charge
  <math|q> and mass <math|m> are moving in the <math|x*y> plane where a
  magntic field along the <math|z> direction is constrained in an infinitely
  long cylinder so that it cannot leak out of it, neither the particles can
  go inside it. However, even if the particles never go in the region where
  <math|<math-bf|B>\<neq\>0>, one can prove that they acquire a phase via the
  potential vector

  <\equation>
    \<phi\>=<frac|q|\<hbar\>><big|oint><rsub|\<partial\>\<Sigma\>><math-bf|A><around*|(|<math-bf|x>|)>\<cdot\>\<mathd\><math-bf|x>=<frac|q|\<hbar\>><big|iint><rsub|\<Sigma\>><math-bf|B><around*|(|<math-bf|x>|)>\<cdot\>\<mathd\><math-bf|s>
  </equation>

  This phase has been measure experimentally in
  [<slink|https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.5.3>]. I
  made this brief digression to highlight the importance of space topology in
  wave propagation. In the AB effect, the presence of a magnetic field
  creates a hole in space. Mathematically speaking, the space in which
  <math|<math-bf|A>> (and therefore <math|<math-bf|B>>) evolves is no longer
  simply connected. There are <math|<math-bf|A>>-circulations that are thus
  non-zero. In this case, I believe we can easily relate it to the
  contractible surfaces I described earlier: a circulation surrounding the
  magnetic field can no longer be reduced to a point.

  <\big-figure|<image|figs/ab_effect.pdf|0.5par|||>>
    <label|ab_effect>Experimental setup to demonstrate the Aharonov-Bohm
    effect: A source emits electrons from the left, and upon reaching the
    screen, an interference pattern emerges, even though the magnetic field
    is seemingly undetectable outside the hatched area. This highlights the
    significance of a quantity responsible for creating the magnetic field,
    which exists throughout space: the vector potential.\ 
  </big-figure>

  <subsubsection|Coriolis force>

  On earth, moving objects are under the influence of the Coriolis force.
  This force is caused by the rotation of the earth<\footnote>
    More generally it is caused by the rotation of the frame.\ 
  </footnote> and its expression when applied to an object of mass <math|m>
  is given by

  <\equation>
    <math-bf|F><rsub|c>=2m*<math-bf|v>*\<times\><math-bf|\<Omega\>><label|coriolis-force>
  </equation>

  where <math|<math-bf|v>> is the velocity of the object and
  <math|<math-bf|\<Omega\>>> the rotation vector of the earth. For the human
  body it is not possible to perceive it on earth, but when placed in a
  rotating platform such as the one of the LEGI lab in Grenoble, coriolis
  force can make any ball game quite impossible, and I speak of experience
  here. Another situation where the Coriolis force is quite visible is when
  observed over a large period of time, like in the well known Foucault
  pendulum <reference|foucault_pendulum> or in situations where the mass is
  big enough to make the force comparable to other forces applying on the
  system, typically for currents or wind.

  <\big-figure|<image|figs/foucault_pendulum.pdf|0.52par|||>>
    <label|foucault_pendulum>The Foucault pendulum is depicted here, with a
    large mass attached to the room's ceiling, chosen to ensure oscillations
    lasting at least an hour, allowing for a noticeable deviation. The
    release of the pendulum was surrounded by a ritual: to prevent any
    incidental movements that could induce rotation in the pendulum, a cord
    holding the mass was burned using a candle after ensuring the perfect
    symmetry of the entire setup. The position of the mass is given by angle
    <math|\<theta\>> and <math|\<phi\>>. Insets shows the Earth with the
    latitude angle <math|\<lambda\>>.
  </big-figure>

  From <reference|coriolis-force> one can see that the Coriolis force depends
  on the latitude and reaches a maximum in intensity at the poles but
  vanishes at the equator. If we place ourselves on the latitude of Paris
  like Foucault did, we launch the pendulum parallel to the equator, we will
  observe a small deviation of the pendulum. This can be seen as the effect
  of the Coriolis force previously described, but it can also be seen from a
  more geometrical point of view. The mass of the pendulum is forced by
  tension and gravity forces to move in a plane tangent to the sphere. But
  the sphere beneath it is rotating, thus after each oscillations, the
  pendulum is moving from one tangent plane to another while <em|trying> to
  stay as parallel as possible to its previous trajectory. This was
  illutrated by the changing color arrows in <reference|zoology> and
  mathematically, it is stated that the vector of the pendulum's velocity
  undergoes parallel transport, on a curved surface. The curvature we are
  talking about here is the Gaussian curvature <math|\<kappa\>=1/R<rsup|2>>
  with <math|R> the radius of the Earth. So far we have seen that when a
  vector undergoes parallel transport on a curved surface, the final vector
  phase does not match the one of the initial vector, the formula that gives
  this phase mismatch is

  <\equation>
    \<phi\>=<big|int><rsub|\<Sigma\>>\<kappa\> \<mathd\>s
  </equation>

  where <math|\<Sigma\>> is the surface enclosed by the path. For our example
  the surface for one day of oscillations appears to be
  <math|2\<pi\><around*|(|1-sin<around*|(|\<lambda\>|)>|)>R<rsup|2>> giving a
  phase mismatch <math|\<phi\>=><math|2\<pi\><around*|(|1-sin<around*|(|\<lambda\>|)>|)>>.

  To summarize what has been covered thus far: the movement of the Foucault
  pendulum follows parallel transport along a curve described by the rotation
  of the Earth. Here, the Earth can be perceived as a closed manifold, and
  the set of tangent planes in which the Foucault pendulum oscillates
  represents a vector bundle over this manifold. The phase induced by the
  curvature of the Earth and the parallel transport on the pendulum's
  velocity vector is known as the holonomy. Further, in quantum physics, the
  considered curvature is referred to as Berry curvature, and the resulting
  phase is termed the Berry phase. Additionally, in quantum physics, the
  tangent planes stem from the eigenvectors of the Hamiltonian. While AB
  effect is created by a modification of the topology of the space (the
  magnetic field can be seen as a hole in the space), it is the curvature of
  the space that creates Coriolis effect.

  <subsubsection|Kelvin waves>

  Kelvin waves are one type of equatorial waves. They are created by the
  force of gravity and are large enough to experience the Coriolis force. One
  example of Kelvin waves are tidal waves, they can be observed in the
  Manche, the sea between France and the UK. There, the amplitudes of waves
  on the French coast are bigger than the ones of the Britannic coast, this
  is due to the Coriolis force that deviates the tidal waves towards the
  east.\ 

  But what is the link between these waves and topological effects such as
  the ones present in quantum hall effect ? It appears that in both
  situations, topologically protected edgesstates can be observed. The Earth
  itself has no edges but if we split it in two along the equator, we end up
  with two caps that have the equator for edge. It is also along the equator
  that the Coriolis force flips its sign, if one experience deviation towards
  the east in the north hemisphere, the diviation goes toward the west in the
  south direction. For this reason, kelvin waves that goes along the equator
  are forced when they encounter a limit, such as a coastline, to go toward
  the south in the north hemisphere, and toward the north in the south
  hemisphere. This property ensure that Kelvin waves are trapped in one of
  the two hemisphere and can't escape of it. This is the typical
  manifestation of a topological behaviour. These waves have also been
  created experimentally in a rotating tanks [S. Sakai, I. Lizawa, E.
  Aramaki, \PGFD Online ressources\Q, https://www.gfd-dennou.org/] and
  theoretically, they have been described in a lot of ressources [Matsuno in
  1966], using a shallow water model where the thickness of the system is
  considered as much smaller than the lenght scale.

  However, the comparison with quantum hall effects stops here, we don't
  observe any quantized value in the physics of Kelvin waves. It is however
  possible to compute topological index as it was shown by recent work on the
  matter.

  https://ahl.centre-mersenne.org/articles/10.5802/ahl.169/

  https://perso.ens-lyon.fr/pierre.delplace/Gazette161.pdf

  \;

  <slink|https://comptes-rendus.academie-sciences.fr/physique/item/10.5802/crphys.28.pdf>

  <slink|https://iopscience.iop.org/article/10.1088/0143-0807/1/3/008>
  classical AB effect\ 

  <\big-figure|<image|figs/kelvin_wave_schema.pdf|0.9par|||>>
    The two large arrows on the Earth represent Kelvin waves near the
    equator. Due to the Coriolis effect, Kelvin waves in the northern
    hemisphere propagate eastward and cannot cross the equator. The inset
    displays marine currents from the website []. On the right, we focus on
    the channel between France and the UK. Here, because of the Coriolis
    effect, tidal waves are stronger on France's beaches compared to those in
    the UK [].

    https://earth.nullschool.net/#2023/11/01/0000Z/ocean/surface/currents/orthographic=206.11,7.17,410
    the first of november 2023.

    https://academic.oup.com/plms/article-abstract/s2-20/1/148/1512263?login=false
  </big-figure>

  \;

  <subsection|Summary>

  In this section, we've covered several examples of wave propagation
  exhibiting topological behavior without resorting to quantum properties.
  We've seen that a key tool to induce topological behavior in waves is to
  break time-reversal symmetry. Additionally, when topological
  characteristics offer protection to waves against disorder or lattice
  imperfections, we've observed wave propagation along the edges of the
  sample or boundaries with materials exhibiting a different topological
  character. In Table <reference|summary_table>, I've summarized the
  experiments we've encountered, along with the various methods used to break
  TRS. Additionally, I've indicated the quantum equivalents of these
  experiments

  <\big-table|<tabular|<tformat|<cwith|1|-1|1|-1|cell-halign|c>|<cwith|1|-1|1|-1|cell-tborder|1ln>|<cwith|1|-1|1|-1|cell-bborder|1ln>|<cwith|1|-1|1|-1|cell-lborder|1ln>|<cwith|1|-1|1|-1|cell-rborder|1ln>|<cwith|1|2|1|-1|cell-hyphen|n>|<cwith|2|2|3|3|cell-background|#a0a0a0>|<table|<row|<cell|>|<cell|Way
  to break TRS>|<cell|Quantum analog>>|<row|<cell|Electron
  gas>|<cell|Magnetic field>|<cell|>>|<row|<cell|Oceanic
  waves>|<cell|Coriolis force>|<cell|QHE>>|<row|<cell|Acoustic
  waves>|<cell|Air flow>|<cell|QHE>>|<row|<cell|Mechanical
  waves>|<cell|Gyroscopic effect>|<cell|QSHE>>>>>>
    <label|summary_table>
  </big-table>

  <section|Bibliography>

  <slink|https://pubs.aip.org/physicstoday/article/63/1/33/413248/The-quantum-spin-Hall-effect-and-topological>
  QSHE

  \;

  <slink|https://www.science.org/doi/10.1126/science.1237215> Complete
  quantum trio

  <slink|https://journals.aps.org/rmp/abstract/10.1103/RevModPhys.91.015006>
  Topological photonic review

  <slink|https://www.sciencedirect.com/science/article/abs/pii/S0081194708604592?via%3Dihub>
  formalism of band theory (1962)

  Voir: <slink|https://spectrum.ieee.org/topological-photonics-what-it-is-why-we-need-it>

  \;

  \;

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|ab_effect|<tuple|5|10>>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|4|9>>
    <associate|auto-11|<tuple|4.2|9>>
    <associate|auto-12|<tuple|4.3|9>>
    <associate|auto-13|<tuple|4.3.1|9>>
    <associate|auto-14|<tuple|5|10>>
    <associate|auto-15|<tuple|4.3.2|10>>
    <associate|auto-16|<tuple|6|11>>
    <associate|auto-17|<tuple|4.3.3|12>>
    <associate|auto-18|<tuple|7|12>>
    <associate|auto-19|<tuple|4.4|13>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-20|<tuple|1|13>>
    <associate|auto-21|<tuple|5|13>>
    <associate|auto-3|<tuple|1|2>>
    <associate|auto-4|<tuple|2|3>>
    <associate|auto-5|<tuple|3|5>>
    <associate|auto-6|<tuple|2|5>>
    <associate|auto-7|<tuple|3|7>>
    <associate|auto-8|<tuple|4|7>>
    <associate|auto-9|<tuple|4.1|7>>
    <associate|coriolis-force|<tuple|8|10>>
    <associate|elem_cells|<tuple|2|5>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|3>>
    <associate|footnote-3|<tuple|3|6>>
    <associate|footnote-4|<tuple|4|10>>
    <associate|footnote-5|<tuple|5|10>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|3>>
    <associate|footnr-3|<tuple|3|6>>
    <associate|footnr-4|<tuple|4|10>>
    <associate|footnr-5|<tuple|5|10>>
    <associate|foucault_pendulum|<tuple|6|11>>
    <associate|newton|<tuple|5|8>>
    <associate|summary_table|<tuple|1|13>>
    <associate|wave-equation|<tuple|1|4>>
    <associate|zoology|<tuple|3|7>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        <with|mode|<quote|math>|<around*|(|a|)>> The classical Hall effect:
        by applying a magnetic field <with|mode|<quote|math>|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>=B*e<rsub|z>>
        on a slab where electrons circulate along the
        <with|mode|<quote|math>|x> axis, the Lorentz force deviate them along
        the <with|mode|<quote|math>|y> axis creating a tension
        <with|mode|<quote|math>|V<rsub|H>> and thus a resistance
        <with|mode|<quote|math>|R<rsub|H>=V<rsub|H>/I>.
        <with|mode|<quote|math>|<around*|(|b|)>> The quantum Hall effect
        (QHE), the setup is similar but at low temperature and the magnetic
        field has to be very strong (1-10 T). Electrons in the bulk are
        blocked in circular orbits, only electrons on the edges can carry a
        current. <with|mode|<quote|math>|<around*|(|c|)>> The spin Hall
        effect, due to intrinsic properties of the material, spin up and down
        are treated differently by spin-orbit interactions, leading to a spin
        accumulation on lateral faces of the material.
        <with|mode|<quote|math>|<around*|(|d|)>> The quantum spin Hall effect
        (QSHE)
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        Decomposition of surfaces into elementary celles (contractible
        surfaces). The circle <with|mode|<quote|math>|<around*|(|a|)>> is
        decomposed in a point and a segment
        <with|mode|<quote|math>|<around*|(|b|)>> wich are both contractible.
        The torus <with|mode|<quote|math>|<around*|(|c|)>> is decomposed in
        an open cylinder and a circle <with|mode|<quote|math>|<around*|(|d|)>>,
        the open cylinder can be reduce to a circle, both circle reduced to
        points and segments. <with|mode|<quote|math>|<around*|(|e|)>>
        presents another decomposition of the torus by cutting y
        transversely, despite being different these two decompostions end up
        computing the same characteristic of Euler:
        <with|mode|<quote|math>|\<chi\><around*|(|Torus|)>=0>.
        <with|mode|<quote|math>|<around*|(|f|)>> presents a decomposition of
        the sphere into elemntary cells.
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        Zoology: <with|mode|<quote|math>|<around*|(|a|)>> Sphere:
        <with|mode|<quote|math>|g=0> <with|mode|<quote|math>|<around*|(|b|)>>
        Torus: <with|mode|<quote|math>|g=1>
        <with|mode|<quote|math>|<around*|(|c|)>> Klein botle:
        <with|mode|<quote|math>|g=2> the Klein bottle is made of two M�bius
        strip sticked together, it is a non orentable-shape and thus the
        genus we refer two is slightly different from the one we define in
        the body of the text. <with|mode|<quote|math>|<around*|(|d|)>> M�bius
        strip is also a non-orientable shape.
        <with|mode|<quote|math>|<around*|(|e|)>> Simple strip.
      </surround>|<pageref|auto-7>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        <with|mode|<quote|math>|<around*|(|a|)>> A ring filled with a fluid,
        such as air in the experiment, allows the propagation of sound. In
        the static state, the ring hosts two counterpropagating modes with
        identical frequencies. However, when the ring rotates at speed
        <with|mode|<quote|math>|v>, the mode moving in the direction of the
        ring's rotation experiences an increase in frequencies, while the
        other mode experiences a decrease. Consequently, this action lifts
        the degeneracy between the two modes.
        <with|mode|<quote|math>|<around*|(|b|)>> An example of honeycomb
        lattice. <with|mode|<quote|math>|<around*|(|c|)>> Masses attached to
        the ground via springs rotate at a frequency of 300Hz due to a DC
        motor. A powerful neodymium magnet creates repulsion between the
        rotating masses, forming a system assembled in a honeycomb lattice.
        <with|mode|<quote|math>|<around*|(|d|)>> A lattice consisting of
        gyroscopes interconnected by springs. Each gyroscope experiences
        three forces from the three springs it's attached to, along with a
        torque resulting from its internal rotation.
      </surround>|<pageref|auto-10>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        Experimental setup to demonstrate the Aharonov-Bohm effect: A source
        emits electrons from the left, and upon reaching the screen, an
        interference pattern emerges, even though the magnetic field is
        seemingly undetectable outside the hatched area. This highlights the
        significance of a quantity responsible for creating the magnetic
        field, which exists throughout space: the vector potential.\ 
      </surround>|<pageref|auto-14>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        The Foucault pendulum is depicted here, with a large mass attached to
        the room's ceiling, chosen to ensure oscillations lasting at least an
        hour, allowing for a noticeable deviation. The release of the
        pendulum was surrounded by a ritual: to prevent any incidental
        movements that could induce rotation in the pendulum, a cord holding
        the mass was burned using a candle after ensuring the perfect
        symmetry of the entire setup. The position of the mass is given by
        angle <with|mode|<quote|math>|\<theta\>> and
        <with|mode|<quote|math>|\<phi\>>. Insets shows the Earth with the
        latitude angle <with|mode|<quote|math>|\<lambda\>>.
      </surround>|<pageref|auto-16>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7>|>
        The two large arrows on the Earth represent Kelvin waves near the
        equator. Due to the Coriolis effect, Kelvin waves in the northern
        hemisphere propagate eastward and cannot cross the equator. The inset
        displays marine currents from the website []. On the right, we focus
        on the channel between France and the UK. Here, because of the
        Coriolis effect, tidal waves are stronger on France's beaches
        compared to those in the UK [].

        https://earth.nullschool.net/#2023/11/01/0000Z/ocean/surface/currents/orthographic=206.11,7.17,410
        the first of november 2023.

        https://academic.oup.com/plms/article-abstract/s2-20/1/148/1512263?login=false
      </surround>|<pageref|auto-18>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-20>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|R�sum�
      en Fran�ais> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Semi-conductors:
      bricks of the modern world> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Localization
      and disorder > <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Topology
      in a nutshell> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>From
      electrons to photons> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8><vspace|0.5fn>

      <with|par-left|<quote|1tab>|4.1<space|2spc>Mechanical waves
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|1tab>|4.2<space|2spc>Acoustic waves
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|1tab>|4.3<space|2spc>Equatorial waves
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>>

      <with|par-left|<quote|2tab>|4.3.1<space|2spc>Aharonov-Bohm effect
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <with|par-left|<quote|2tab>|4.3.2<space|2spc>Coriolis force
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15>>

      <with|par-left|<quote|2tab>|4.3.3<space|2spc>Kelvin waves
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-17>>

      <with|par-left|<quote|1tab>|4.4<space|2spc>Summary
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-19>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|5<space|2spc>Bibliography>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-21><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>