import matplotlib.pyplot as plt
import csv
import sys

sys.path.append('../../chapter-2-thesis/code')
import tex
tex.useTex()

# Liste pour stocker les données
years = []
results = []

# Lecture du fichier CSV
with open('../data/entries.csv', 'r') as file:
    reader = csv.reader(file)
    # next(reader)  # Ignorer l'en-tête s'il y en a un
    for row in reader:
        years.append(int(row[0]))
        results.append(int(row[1]))

# Tracé des données
plt.plot(years, results, marker='+', color='blue',ls="",ms=10)
plt.xlabel(r'$\mathrm{Years}$', fontsize=20)
plt.ylabel(r'$\textrm{Google Scholar Results}$', fontsize=20)
# plt.title('Google Scholar Results Over Years', fontsize=14)
# plt.tick_params(axis='both', which='major', labelsize=20)
plt.xticks(fontsize=20,rotation=45)
plt.yticks(fontsize=20)
plt.grid(True)

plt.savefig("../figs/growing_field.pdf",format="pdf",bbox_inches='tight')


# Affichage du graphique
plt.show()
