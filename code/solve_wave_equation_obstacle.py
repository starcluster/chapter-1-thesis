import numpy as np
import matplotlib.pyplot as plt

# Paramètres de la grille
L = 20.0  # Longueur de la grille en unités
T = 20.0  # Temps total de simulation
Nx = 300  # Nombre de points en x
Nt = 600  # Nombre de pas de temps
dx = L / Nx
dt = T / Nt

# Création de la grille spatiale
x = np.linspace(0, L, Nx)
y = np.linspace(0, L, Nx)
X, Y = np.meshgrid(x, y)

# Initialisation de la fonction d'onde et de son évolution temporelle
psi = np.zeros((Nx, Nx))
psi_new = np.zeros((Nx, Nx))

# Conditions initiales (par exemple, une onde gaussienne centrée)
x0, y0 = L / 4, L / 2
sigma = 0.1
psi = 2*np.exp(-((X - x0) ** 2 + (Y - y0) ** 2) / (2 * sigma ** 2))

def gen_obstacle(X,Y):
    obstacle_radius = 0.1
    obstacle_mask_total = X**2+Y**2 < 0
    for i in range(100):
        obstacle_center = (L / 2+np.random.randint(-10,10)/5, L / 2 +np.random.randint(-10,10)/5)
        obstacle_center = np.array([L/2+(i//10),L/2+(i-(i//10)*10)])/2
        obstacle_mask = ((X - obstacle_center[0]) ** 2 + (Y - obstacle_center[1]) ** 2) <= (
            obstacle_radius ** 2
        )
        obstacle_mask_total += obstacle_mask

    return obstacle_mask_total


obstacle_mask = gen_obstacle(X,Y)
psi[obstacle_mask] = 0.0

# Paramètre de l'équation d'onde (vitesse de la lumière)
c = 1.0

# Boucle temporelle pour résoudre l'équation d'onde
for n in range(Nt):
    for i in range(1, Nx - 1):
        for j in range(1, Nx - 1):
            psi_new[i, j] = 2 * psi[i, j] - psi_new[i, j] + (
                (c ** 2) * (dt ** 2) / (dx ** 2)
            ) * (
                psi[i + 1, j]
                + psi[i - 1, j]
                + psi[i, j + 1]
                + psi[i, j - 1]
                - 4 * psi[i, j]
            )

    psi, psi_new = psi_new, psi  # Mettre à jour psi pour le pas de temps suivant
    psi[obstacle_mask] = 0.0
    # Affichage de la fonction d'onde à chaque 10 pas de temps
    if n % 10 == 0:
        fig, ax = plt.subplots()
        im = ax.imshow(psi, extent=(0, L, 0, L), origin="lower", cmap="viridis",vmin=-2,vmax=2)
        cbar = plt.colorbar(im, ax=ax, orientation='vertical', shrink=0.7, pad=0.03, aspect=20, extend='both', boundaries=np.linspace(-2, 2, 21), ticks=np.linspace(-2, 2, 5))
        cbar.set_label('Valeur')
        cbar.ax.tick_params(labelsize=12)
        # plt.colorbar(label="Amplitude")
        ax.imshow(~obstacle_mask, cmap='gray', extent=(0, L, 0, L), origin='lower', alpha=0.4)

        ax.set_title(f"Évolution temporelle, t = {n * dt:.2f}")
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.axis((5,15,5,15))
        fig.savefig("../figs/" + str(n) + ".png")
        plt.cla()
        plt.clf()
        # plt.show()
