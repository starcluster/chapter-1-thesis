<TeXmacs|2.1>

<style|generic>

<\body>
  I now provide details on the connection that can be made between this
  formalism and the tools used by physicists. For that, I focus on a simple
  two dimensionnal tight-binding model on an infinite periodic lattice. In
  this situation, it is possible to exploit the periodicity of the lattice to
  perform an analysis in Fourier space. The Hamiltonian describing the
  problem is written <math|H>, we can deduce from it a <math|2N\<times\>2N>
  Bloch Hamiltonian <math|<wide|H|^><around*|(|<math-bf|k>|)>> where
  <math|><strong|k> is a quasimomentum that traverses the Brillouin zone
  (BZ), the reciprocal space of the lattice upon which the tight-binding
  model is defined. The parameter <math|N> accounts for the electronic degree
  of freedom in the unit cell (the number of sites, orbitals or spins). To
  this Hamiltonian we can associate eigen-energies <math|E<rsub|n>> and
  eigenstates <math|\<psi\><rsub|n><around*|(|<math-bf|k>|)>>. Note that for
  a given <math|n>, <math|E<rsub|n><around*|(|<math-bf|k>|)>> defines an
  energy band when <math|<math-bf|k>> evolves in the Brillouin zone. The
  Hilbert space <math|\<cal-H\>> describing this problem can be written as a
  direct integral of Hilbert spaces <math|\<cal-H\><rsub|<math-bf|k>>>, the
  continuous equivalent of a direct sum. All
  <math|\<cal-H\><rsub|<math-bf|k>>> are copies of the same space but
  attached to different point <math|<math-bf|k>> of the Brillouin zone.\ 

  This way of seeing things reminds us a lot of what was previously explained
  on fiber bundles. Indeed we can now define the Bloch bundle as
  <math|E<above|\<longrightarrow\>|\<pi\>>BZ> where the total space <math|E>
  is the disjoint union of all the <math|\<cal-H\><rsub|<math-bf|k>>> and the
  Brillouin zone is playing the role of base space. The projection
  <math|\<pi\>> maps elements <math|<around*|(|<math-bf|k>,\<psi\><around*|(|<math-bf|k>|)>|)>>
  to <math|<math-bf|k>> where <math|\<psi\><around*|(|<math-bf|k>|)>\<in\>\<cal-H\><rsub|<math-bf|k>>>
  is obtained by finding the eigenstates of
  <math|<wide|H|^><around*|(|<math-bf|k>|)>>. Note that this fiber bundle is
  always trivial (this is true at least in dimension of space
  <math|d\<leqslant\>3> see []). Let's now define two subbundles over two
  different total space. We assume that the energies are separated in two
  distinct groups by an energy gap, the energies below the gap defined the
  conduction bundle <math|E<rsup|-><above|\<longrightarrow\>|\<pi\><rsup|->>BZ>
  and those above it the valence bundle <math|E<rsup|+><above|\<longrightarrow\>|\<pi\><rsup|+>>BZ>.
  Then these fiber bundles can be non-trivial. It is in this situation that
  the system will exhibit topological features. The Chern number, defined in
  [], tells if the two subbundles are trivial or not. If they are not trivial
  then there is an obstruction for each application
  <math|\<pi\><rsup|\<pm\>>> to continuously map the eigenstates to the
  entire Brillouin zone.

  \;

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>