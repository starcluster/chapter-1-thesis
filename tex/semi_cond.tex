\section{Semi-conductors: bricks of the modern world}

%% Former section on semi cond, the part on QHE was used.

\textcolor{myred}{TODO}:
https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.49.405 expliquer
que la formule pour l'indice de chern était DÉJÀ présente même sans
parler de topologie


Before diving into the realm of topology, let's focus a bit on another
vast field of physics: semi-conductors. Because of their presence in
transistors, they are now everywhere, phones, computers, cars,
... They are fundamental to build solar pannels, LED, photo-detectors
and more generally any kind of computers. Because of this omnipresence
they are the essential bricks of the modern world and considered to be
\textit{the most frequently manufactured human artifact in history}
with a number of transistor estimated to approach the number of human
cells on the Earth \cite{Handy2014May}.

Wondering what a semi-conductor is leads us to wondering what a
conductor is.  One could simply say that a conductor \say{is not an
  insulator}. As I did not defined an insulator, this definition would
be pointless, so we should remember this fact more as a property than
a proper definition. Some materials are known to carry currents. This
is the case of metal per example. A very naive model to describe the
transport of currents through a metal is the Drude model
\cite{Drude1900Jan}. In this model, electrons move freely in the
metal, following the laws of classical mechanics, bouncing against the
ions of the metal like in a pinball machine. The more impureties are
present in the metal, the more they are slowed down, making the metal
less conductive, and thus more resistant. Surprisingly the relation
between the amount of impurities and the conductivity is not linear
and drops drastically at a certain rate of impurities, making the
metal isolant. The reason behind this phenomenum is called Anderson
localization \cite{Anderson1958Mar} and is not the main subject of
this work, but we might touch upon it from time to time. To sum up:
electrical conductors are materials that allow current to flow by
enabling charge carriers to move, whereas in insulators, nothing
happens, and no current flows. Of course this definition is not
perfect and would not suit up to exotic situations but it is a complex
enough one regarding the requirements of this work.

Now that we rule on what \say{insulators} and \say{conductors} are, we
can focus on semi-conductors that are sometimes conductors, sometimes
insulators.  Their nature depends often on a physical parameter such
as temperature or internal disorder. This feature of semi-conductor to
let or not pass the current allows for a very wide range of
applications, the most known being probably transistors where a small
electric field can control a way bigger current. Here, the electric
field plays a role at an atomic scale, to understand what happens we
need to forget about the Drude model and have a look at what quantum
mechanics says. We know that the energy of an atom is quantified, only
precise values of energy are allowed for an atom, per example $E_1,
E_2, \ldots, E_n$. When many atoms are put together, these precise
values tend to enlarge and the ensemble can take values in intervals:
$\Delta E_1, \ldots, \Delta E_n$. These intervals are called
\say{bands} \cite{Blount1962Jan} and the forbidden energy ranges
between bands are called \say{band gaps} or simply \say{gaps}. As we
want to increase the conductivity of our materials, we want to be sure
that an electron will be allowed to go through it. I introduce here
the concept of \say{Fermi level} as the exchange of energy required to
add one electron to the system. In a semi-conductor, the band that is
situated just below the Fermi level is called the valence band and the
one above is called the conduction band and these two bands are
separated by a gap that is small enough so that applying a small
electric field will be enough to move electrons from the valence band
to the conduction band allowing the current to flow.

\begin{figure}[h]\centering
  \resizebox{0.99\columnwidth}{!}{\includegraphics{hall_effect.pdf}}
  \caption{$(a)$ The classical Hall effect: by applying a magnetic field
  $\mathbf{B} = Be_z$ on a slab where electrons circulate along the $x$ axis,
  the Lorentz force deviate them along the $y$ axis creating a tension $V_H$
  and thus a resistance $R_H = V_H / I$. $(b)$ The quantum Hall effect (QHE),
  the setup is similar but at low temperature and the magnetic field has to be
  very strong (1-10 T). Electrons in the bulk are blocked in circular orbits,
  only electrons on the edges can carry a current. $(c)$ The spin Hall effect,
  due to intrinsic properties of the material, spin up and down are treated
  differently by spin-orbit interactions, leading to a spin accumulation on
  lateral faces of the material. $(d)$ The quantum spin Hall effect (QSHE)}
\end{figure}

So far, we have discussed materials whose insulating or conducting character
evolves over time, but there are also materials that are both insulating and
conducting in different regions of space. Often, they are insulating in the
bulk, which is the core or interior of the sample, and conducting at the
edges. These are known as topological insulators. For the moment, it is not at
all obvious what makes them topological. We will address this later. Let's
focus on the description of the most well-known of these phenomena: the
quantum Hall effect (QHE) with first a small detour by the Hall effect.


The Hall effect was discovered by Edwin Hall in 1879. A current $I$
goes through a conducting material, carried by electrons at velocity $
\mathbf{v}$.  Then, a perpendicular magnetic field $\mathbf{B}$ is
applied to the current.  The Lorentz force deviates the electrons in a
direction perpendicular to the magnetic field, consequence of the
expression of the Lorentz force: $\mathbf{F} = q \mathbf{v} \times
\mathbf{B}$. This deviation creates an electric field and then a Hall
voltage $V_H$ that can be measured to retrieve the intensity of the
magnetic field. I also introduce here another quantity: the Hall
resistance $R_H = V_H / I$.

The QHE is quite similar: we take the same setup but we now consider electrons
located within a plane. Some authors refer to this plane as a ``two dimensionnal
electron gas'' (2DEG). The experiment also takes place at very low temperature
(near 0K) and strong magnetic fields are required. Because of the strong
magnetic field, electrons are forced to follow circular orbits\footnote{Note
that this is quite important for the electrons to be confined in 2D otherwise
they would just leave the sample in a cyclotron motion.} and their energy is
now quantized: it can only take equidistant values called Landau levels. The
consequence of this quantization is that the Hall resistance is now quantized
and equals $h / e^2 \nu$ where $h$ is the Planck constant, $e$ the elementary
charge and $\nu$ is an integer. As the electrons in the bulk are blocked in
circular orbits, they cannot be used as charged carriers, thus only the
electrons at the edges can play this role. The conducting states are localized
on the edge of the sample. This property is highly intriguing as it ensures
that electron transport will occur without backscattering, and heat losses
will be low to the point of being negligible. The reason for this last point
to happen is that if the electrons are allowed to move only in one direction
then there is no interaction with backscattered electrons, yet this is the
reason of heat loss.

Despite being very interesting, the QHE is hard to achieve outside of
a laboratory. The strong magnetic fields required could not be created
in small devices. One avenue considered to eliminate the need for
these intense magnetic fields is the quantum spin Hall effect
(QSHE) \cite{Qi2010Jan,Oh2013Apr}. By using particles that possess both spin up
and spin down, we can take advantage of spin-orbit coupling to
``separate'' the paths of up and down particles.
