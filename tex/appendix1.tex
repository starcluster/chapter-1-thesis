\section{Vector bundles and fiber bundles}
\label{sec:appendix_topo}

Here we present some more formal definitions regarding vector bundles,
followed by an explanation of their use in physics. The content of
this section is based on \cite{VBbook}, \cite{Nakahara2003Jun} and
\cite{Bradlyn2022May}.

A $n$-dimensional vector bundle is a continuous function (a map)
$p:\mathcal{E}\rightarrow\mathcal{B}$ which verifies that for every
$b$ in $\mathcal{B}$, $p^{-1}(b)$ has the structure of a vector
space. We also assume that there is a cover of $\mathcal{B}$,
\textit{i.e.} a family of subsets of $\mathcal{B}$, made of open sets
$U_{\alpha}$ such that $p^{-1}(U_{\alpha})$ is homeomorph to
$U_{\alpha}\times \mathbb{R}^n$. This condition is called the local
triviality condition. The vector bundles are a special case of fiber
bundles with this triviality condition. A way to see this is to zoom
in enough on $\mathcal{B}$, so that we can describe it locally by a vector
space $\mathbb{R}^n$. $\mathcal{B}$ is called the base and we will
consider it to be a smooth manifold even if the definition of vector
bundle can be wider. \cref{fig:fiber_bundle} illustrates in an
abstract way the concepts defined in this section.

As an example we can consider the sphere $\mathcal{S}^2$ and the set
of tangent spaces $\mathcal{T}\mathcal{S}^2$. We define the application
$h:(x,\tau(v_x))\mapsto x$ where $\tau(v_x)$ is the tangent space to
the sphere $\mathcal{S}^2$ at point $x$ transported to the point
$0$. $h$ is a very simple example of vector bundle where we can
identify the base as the sphere $\mathcal{S}^2$ and the total space as
$\mathcal{S}^2 \times \mathbb{R}^3$.

An even simpler example would be to choose $p:\mathcal{B}\times
\mathbb{R}^n \rightarrow \mathcal{B}$ with $p$ the projection onto the
first factor: $\mathcal{B}$. This vector bundle is called the trivial
bundle. A fiber bundle will be called trivial if it happens to be
homeomorph to this trivial bundle.

On the other hand, for an interval $I$ if we define the relation of
equivalence $(0,t) \sim (1,-t)$, then the projection $p:I\times
\mathbb{R} \rightarrow I$ under the relation of equivalence previously
defined is known as the Möbius bundle.

Another important notion when it comes to speak of vector (or fiber)
bundles is a section. Consider a map $p:E\rightarrow \mathcal{B}$ that
happens to be a vector bundle. A section of it is also a map
$s:\mathcal{B} \rightarrow E$ that for each $b$ of the base
$\mathcal{B}$, assigns a corresponding vector $s(b)$ in the vector
space $p^{-1}(b)$. We have $ps = \mathds{1}$. Note that the section is
the inverse of the projection map only if the fiber bundle is trivial.

Proper definitions of direct sums, inner products or even tensor
products can be given for vector bundles. I will not reproduce them
here because they do not have any unexpected features. Consult chapter
1 of \cite{VBbook} for more details.

I now provide details on the connection that can be made between this
formalism and the tools used by physicists. For that, I focus on a
simple two dimensionnal tight-binding model on an infinite periodic
lattice. In this situation, it is possible to exploit the periodicity
of the lattice to perform an analysis in Fourier space. The
Hamiltonian describing the problem is written $H$, we can deduce from
it a $N \times N$ Bloch Hamiltonian $\hat{H} (\mathbf{k})$ where
{\tmstrong{k}} is the momentum in the Brillouin zone (BZ), the
reciprocal space of the lattice upon which the tight-binding model is
defined. The parameter $N$ accounts for the electronic degrees of
freedom in the unit cell (the number of sites, orbitals or spins).  To
this Hamiltonian we can associate eigenenergies $E_n$ and eigenstates
$\bm{\psi}_n (\mathbf{k})$. Note that for a given $n$, $E_n
(\mathbf{k})$ defines an energy band when $\mathbf{k}$ evolves in the
BZ. The Hilbert space $\mathcal{H}$ describing this problem can be
written as a direct integral of Hilbert spaces
$\mathcal{H}_{\mathbf{k}}$, the continuous equivalent of a direct
sum. All $\mathcal{H}_{\mathbf{k}}$ are copies of the same space but
attached to different points $\mathbf{k}$ of the Brillouin zone.

This way of seeing things reminds us a lot of what was previously
explained for fiber bundles. Indeed we can now define the Bloch bundle
as $E \overset{\pi}{\longrightarrow} \tmop{BZ}$ where the total space
$E$ is the disjoint union of all the $\mathcal{H}_{\mathbf{k}}$ and
the Brillouin zone is playing the role of base space. The projection
$\pi$ maps elements $(\mathbf{k}, \bm{\psi} (\mathbf{k}))$ to
$\mathbf{k}$ where $\bm{\psi} (\mathbf{k}) \in
\mathcal{H}_{\mathbf{k}}$ is obtained by finding the eigenstates of
$\hat{H} (\mathbf{k})$. Note that this fiber bundle is always trivial
(this is true at least in dimension of space $d \leqslant 3$ see
\cite{Simon1983Dec}). Let's now define two subbundles over two
different total spaces. We assume that the energies are separated in
two distinct groups by an energy gap, the energies below the gap
define the conduction bundle $E^- \overset{\pi^-}{\longrightarrow}
\tmop{BZ}$ and those above it $\mbox{--}$ the valence bundle $E^+
\overset{\pi^+}{\longrightarrow} \tmop{BZ}$. Then these fiber bundles
can be non-trivial. It is in this situation that the system exhibits
topological features. The Chern number, defined in
\cref{sec:chapter2}, tells whether the two subbundles are trivial or
not. If they are not trivial then there is an obstruction for each
application of $\pi^{\pm}$ to continuously map the eigenstates to the
entire Brillouin zone. The Chern number is defined by integrating the
Berry curvature over the Brillouin zone. The Berry curvature is
defined from the Berry connection which is itself defined from the
section $s^-:\vec{k}\mapsto \bm{\psi}_n (\vec{k})$ and its exterior
derivative. This section associates to the Brillouin zone the
eigenstates of the conduction band. For a better understanding on how
the Chern number constitues an obstruction, I recommend reading
\cite{Fruchart2013Nov}.


\begin{figure}[h]\centering
  \resizebox{0.9\columnwidth}{!}{\includegraphics{../figs/fiber_bundle.pdf}}
  \caption{Symbolic representation of the various concepts involved
    when discussing a vector bundle. $\mathcal{E}$ represents the
    total space, $\mathcal{B}$ denotes the base space, typically a
    manifold. The map $p:\mathcal{E}\rightarrow\mathcal{B}$ simply
    generates the fibers $p^{-1}(b)$, which are vector spaces created
    from the vectors of $\mathcal{B}$. The sets of $U_{\alpha}$ are
    open sets covering $\mathcal{B}$ and locally $p^{-1}(U_{\alpha})$
    is homeomorphic to $U_{\alpha}\times \mathbb{R}^n$, describing a
    local trivialization of the
    bundle. $s:\mathcal{B}\rightarrow\mathcal{E}$ represents a section
    of the fibers such that $s(x)=s_x \in p^{-1}(x)$ which transports
    every vector $x$ of $\mathcal{B}$ to a vector in
    $p^{-1}(x)$.\label{fig:fiber_bundle}}
\end{figure}
